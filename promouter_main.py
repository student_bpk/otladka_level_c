# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'promouter_main.ui'
#
# Created by: PyQt5 UI code generator 5.15.4
#
# WARNING: Any manual changes made to this file will be lost when pyuic5 is
# run again.  Do not edit this file unless you know what you are doing.


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_PromouterMain(object):
    def setupUi(self, PromouterMain):
        PromouterMain.setObjectName("PromouterMain")
        PromouterMain.resize(400, 300)
        self.label = QtWidgets.QLabel(PromouterMain)
        self.label.setGeometry(QtCore.QRect(100, 20, 201, 31))
        font = QtGui.QFont()
        font.setPointSize(14)
        self.label.setFont(font)
        self.label.setObjectName("label")

        self.retranslateUi(PromouterMain)
        QtCore.QMetaObject.connectSlotsByName(PromouterMain)

    def retranslateUi(self, PromouterMain):
        _translate = QtCore.QCoreApplication.translate
        PromouterMain.setWindowTitle(_translate("PromouterMain", "Главная"))
        self.label.setText(_translate("PromouterMain", "Модуль промоутера"))
