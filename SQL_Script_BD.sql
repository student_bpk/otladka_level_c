-- MySQL Script generated by MySQL Workbench
-- Fri Apr 22 09:22:17 2022
-- Model: New Model    Version: 1.0
-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema atrokhovd
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema atrokhovd
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `atrokhovd` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci ;
-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
USE `atrokhovd` ;

-- -----------------------------------------------------
-- Table `atrokhovd`.`Klienty_agenstva`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `atrokhovd`.`Klienty_agenstva` (
  `idKlienty_agenstva` INT NOT NULL AUTO_INCREMENT,
  `Familiya` VARCHAR(45) NOT NULL,
  `Client's_NAME` VARCHAR(45) NOT NULL,
  `Otchestvo` VARCHAR(45) NOT NULL,
  `Data_rogdeniya` DATE NOT NULL,
  `Name_Organizations` VARCHAR(45) NOT NULL,
  `Dolgnost` VARCHAR(45) NOT NULL,
  `Passport_data` VARCHAR(45) NOT NULL,
  `Mobile_phone` VARCHAR(45) NOT NULL,
  `Nomer_dogovora_ob_okazanii_uslyg` INT NOT NULL,
  PRIMARY KEY (`idKlienty_agenstva`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;


-- -----------------------------------------------------
-- Table `atrokhovd`.`Manager`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `atrokhovd`.`Manager` (
  `idManager` INT NOT NULL AUTO_INCREMENT,
  `Familiya` VARCHAR(45) NOT NULL,
  `Manager's_Name` VARCHAR(45) NOT NULL,
  `Otchestvo` VARCHAR(45) NOT NULL,
  `Email` VARCHAR(45) NOT NULL,
  `Mobile_phone` VARCHAR(45) NOT NULL,
  `Login` VARCHAR(45) NOT NULL,
  `Password` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`idManager`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;


-- -----------------------------------------------------
-- Table `atrokhovd`.`Promoakcionnye_uslugi_firms`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `atrokhovd`.`Promoakcionnye_uslugi_firms` (
  `idPromoakcionnye_uslugi_firms` INT NOT NULL AUTO_INCREMENT,
  `Uslugi_agenstva` VARCHAR(45) NOT NULL,
  `Stepen'_uroven'_slognosti` VARCHAR(45) NOT NULL,
  `Sposoby_formy_provedeniya` VARCHAR(45) NOT NULL,
  `Edinica_izmereniya_stoimosti` VARCHAR(45) NOT NULL,
  `Stoimost'` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`idPromoakcionnye_uslugi_firms`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;


-- -----------------------------------------------------
-- Table `atrokhovd`.`Promoter`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `atrokhovd`.`Promoter` (
  `idPromoter` INT NOT NULL AUTO_INCREMENT,
  `Familiya` VARCHAR(45) NOT NULL,
  `Name_of_the_promoter` VARCHAR(45) NOT NULL,
  `Otchestvo` VARCHAR(45) NOT NULL,
  `Email` VARCHAR(45) NOT NULL,
  `Mobile_phone` VARCHAR(45) NOT NULL,
  `Login` VARCHAR(45) NOT NULL,
  `Password` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`idPromoter`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;


-- -----------------------------------------------------
-- Table `atrokhovd`.`Supervisors`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `atrokhovd`.`Supervisors` (
  `idSupervisors` INT NOT NULL AUTO_INCREMENT,
  `Familiya` VARCHAR(45) NOT NULL,
  `Supervisor's_name` VARCHAR(45) NOT NULL,
  `Otchestvo` VARCHAR(45) NOT NULL,
  `Email` VARCHAR(45) NOT NULL,
  `Mobile_phone` VARCHAR(45) NOT NULL,
  `Login` VARCHAR(45) NOT NULL,
  `Password` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`idSupervisors`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;


-- -----------------------------------------------------
-- Table `atrokhovd`.`Zakazy_BTL_promo_meropriyatiy`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `atrokhovd`.`Zakazy_BTL_promo_meropriyatiy` (
  `idZakazy_BTL_promo_meropriyatiy` INT NOT NULL AUTO_INCREMENT,
  `Data_zaklycheniya_dogovora` DATE NOT NULL,
  `Klient` INT NOT NULL,
  `Tip_promoakcii_firmy` INT NOT NULL,
  `Mesto_provedeniya` VARCHAR(45) NOT NULL,
  `Prodolgitelnost_akcii` VARCHAR(45) NOT NULL,
  `Kolichestvo_zadeistvovannyh_sotrydnikov` INT NOT NULL,
  `Inye_ysloviya` VARCHAR(45) NOT NULL,
  `Manager` INT NOT NULL,
  `Supervisor` INT NOT NULL,
  `Promoter` INT NOT NULL,
  `SUMMA_OPLATY` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`idZakazy_BTL_promo_meropriyatiy`),
  INDEX `Klient_idx` (`Klient` ASC) VISIBLE,
  INDEX `Promoakciya_idx` (`Tip_promoakcii_firmy` ASC) VISIBLE,
  INDEX `Manager_idx` (`Manager` ASC) VISIBLE,
  INDEX `Supervisors_idx` (`Supervisor` ASC) VISIBLE,
  INDEX `Promoter_idx` (`Promoter` ASC) VISIBLE,
  CONSTRAINT `Klient`
    FOREIGN KEY (`Klient`)
    REFERENCES `atrokhovd`.`Klienty_agenstva` (`idKlienty_agenstva`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `Manager`
    FOREIGN KEY (`Manager`)
    REFERENCES `atrokhovd`.`Manager` (`idManager`),
  CONSTRAINT `Promoakciya`
    FOREIGN KEY (`Tip_promoakcii_firmy`)
    REFERENCES `atrokhovd`.`Promoakcionnye_uslugi_firms` (`idPromoakcionnye_uslugi_firms`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `Promoter`
    FOREIGN KEY (`Promoter`)
    REFERENCES `atrokhovd`.`Promoter` (`idPromoter`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `Supervisors`
    FOREIGN KEY (`Supervisor`)
    REFERENCES `atrokhovd`.`Supervisors` (`idSupervisors`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;


-- -----------------------------------------------------
-- Table `atrokhovd`.`Spisok_shtrafov`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `atrokhovd`.`Spisok_shtrafov` (
  `idSpisok_shtrafov` INT NOT NULL AUTO_INCREMENT,
  `Spisok_shtrafov` VARCHAR(45) NOT NULL,
  `Sankcii_vzyskanie` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`idSpisok_shtrafov`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `atrokhovd`.`Kontrol'_raboty_promouterov`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `atrokhovd`.`Kontrol'_raboty_promouterov` (
  `idKontrol'_raboty_promouterov` INT NOT NULL AUTO_INCREMENT,
  `Manager` INT NOT NULL,
  `Supervisors` INT NOT NULL,
  `Promoter` INT NOT NULL,
  `Data_vremya_kontrolya` DATE NOT NULL,
  `Mesto_promoakcii_tochka` VARCHAR(45) NOT NULL,
  `Prisutstvie_personala_na_rabochem_meste` TINYINT NULL,
  `Stepen'_aktivnosti_raboty_promoutera` VARCHAR(45) NOT NULL,
  `Vyyavlennye_narusheniya` VARCHAR(45) NOT NULL,
  `Shtraf` INT NULL,
  PRIMARY KEY (`idKontrol'_raboty_promouterov`),
  INDEX `Manager_idx` (`Manager` ASC) VISIBLE,
  INDEX `Supervisors_idx` (`Supervisors` ASC) VISIBLE,
  INDEX `Promoter_idx` (`Promoter` ASC) VISIBLE,
  INDEX `Shtraf_idx` (`Shtraf` ASC) VISIBLE,
  CONSTRAINT `Manager`
    FOREIGN KEY (`Manager`)
    REFERENCES `atrokhovd`.`Manager` (`idManager`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `Supervisors`
    FOREIGN KEY (`Supervisors`)
    REFERENCES `atrokhovd`.`Supervisors` (`idSupervisors`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `Promoter`
    FOREIGN KEY (`Promoter`)
    REFERENCES `atrokhovd`.`Promoter` (`idPromoter`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `Shtraf`
    FOREIGN KEY (`Shtraf`)
    REFERENCES `atrokhovd`.`Spisok_shtrafov` (`idSpisok_shtrafov`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `atrokhovd`.`Spisok_akcij_promoutera`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `atrokhovd`.`Spisok_akcij_promoutera` (
  `idSpisok_akcij_promoutera` INT NOT NULL AUTO_INCREMENT,
  `Data_nachala_promoakcii` DATE NOT NULL,
  `Data_okonchaniya_promoakcii` DATE NOT NULL,
  `Promoakciya_firmy` INT NOT NULL,
  `Promoter` INT NOT NULL,
  `Mesto_provedeniya` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`idSpisok_akcij_promoutera`),
  INDEX `Promoakciya_idx` (`Promoakciya_firmy` ASC) VISIBLE,
  INDEX `Promoter_idx` (`Promoter` ASC) VISIBLE,
  CONSTRAINT `Promoakciya`
    FOREIGN KEY (`Promoakciya_firmy`)
    REFERENCES `atrokhovd`.`Promoakcionnye_uslugi_firms` (`idPromoakcionnye_uslugi_firms`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `Promoter`
    FOREIGN KEY (`Promoter`)
    REFERENCES `atrokhovd`.`Promoter` (`idPromoter`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `atrokhovd`.`Spisok_dostupnyh_akcij`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `atrokhovd`.`Spisok_dostupnyh_akcij` (
  `idSpisok_dostupnyh_akcij` INT NOT NULL AUTO_INCREMENT,
  `Promoakciya_firmy` INT NOT NULL,
  `Data_nachala` DATE NOT NULL,
  `Mesto_provedeniya` VARCHAR(45) NOT NULL,
  `Kolichestvo_promouterov_uchastnikov` INT NOT NULL,
  PRIMARY KEY (`idSpisok_dostupnyh_akcij`),
  INDEX `Promoakciya_idx` (`Promoakciya_firmy` ASC) VISIBLE,
  CONSTRAINT `Promoakciya`
    FOREIGN KEY (`Promoakciya_firmy`)
    REFERENCES `atrokhovd`.`Promoakcionnye_uslugi_firms` (`idPromoakcionnye_uslugi_firms`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
