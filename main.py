from multiprocessing.sharedctypes import Value
from PyQt5 import QtWidgets
import autorization  # Это наш конвертированный файл дизайна
import registration
import promouter_main
import sys 

class promouterMain(QtWidgets.QMainWindow,promouter_main.Ui_PromouterMain):
    def __init__(self):
        # Это здесь нужно для доступа к переменным, методам
        # и т.д. в файле autotization..py
        super().__init__()
        self.setupUi(self) 
class Reg(QtWidgets.QMainWindow,registration.Ui_registration):
    def __init__(self):
        # Это здесь нужно для доступа к переменным, методам
        # и т.д. в файле autotization..py
        super().__init__()
        self.setupUi(self) 

class Auth(QtWidgets.QMainWindow, autorization.Ui_autorization):
    def __init__(self):
        # Это здесь нужно для доступа к переменным, методам
        # и т.д. в файле autotization..py
        super().__init__()
        self.setupUi(self)  # Это нужно для инициализации нашего дизайна
        self.ButtonOK.clicked.connect(self.copy_pass)
        self.ButtonCancel.clicked.connect(self.close_app)
        self.LinkButton.clicked.connect(self.action_registration)

    def copy_pass(self):
       parol = self.LinePassword.text()
       print(f'Что там? Вот что: {parol}')
       self.LinePassword.clear()
       self.promouterMain=promouterMain()
       self.promouterMain.show()
       
    def close_app(self):
        sys.exit()
        
    def action_registration(self):
        
        self.winReg=Reg()
        self.winReg.show()
       



def main():
    app = QtWidgets.QApplication(sys.argv)  # Новый экземпляр QApplication
    window = Auth()  # Создаём объект класса ExampleApp
    window.show()  # Показываем окно
    app.exec_()  # и запускаем приложение


   
if __name__ == '__main__':  # Если мы запускаем файл напрямую, а не импортируем
    main()  # то запускаем функцию main()